package com.dgha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimeraTareaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimeraTareaApplication.class, args);
	}

}
